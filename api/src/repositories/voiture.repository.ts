import {DefaultCrudRepository} from '@loopback/repository';
import {Voiture, VoitureRelations} from '../models';
import {MydatasourceDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class VoitureRepository extends DefaultCrudRepository<
  Voiture,
  typeof Voiture.prototype.matricule,
  VoitureRelations
> {
  constructor(
    @inject('datasources.mydatasource') dataSource: MydatasourceDataSource,
  ) {
    super(Voiture, dataSource);
  }
}
