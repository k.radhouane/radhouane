import {DefaultCrudRepository} from '@loopback/repository';
import {Facture, FactureRelations} from '../models';
import {MydatasourceDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FactureRepository extends DefaultCrudRepository<
  Facture,
  typeof Facture.prototype.id,
  FactureRelations
> {
  constructor(
    @inject('datasources.mydatasource') dataSource: MydatasourceDataSource,
  ) {
    super(Facture, dataSource);
  }
}
