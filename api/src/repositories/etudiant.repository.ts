import {DefaultCrudRepository} from '@loopback/repository';
import {Etudiant, EtudiantRelations} from '../models';
import {MydatasourceDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EtudiantRepository extends DefaultCrudRepository<
  Etudiant,
  typeof Etudiant.prototype.cin,
  EtudiantRelations
> {
  constructor(
    @inject('datasources.mydatasource') dataSource: MydatasourceDataSource,
  ) {
    super(Etudiant, dataSource);
  }
}
