import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Facture} from '../models';
import {FactureRepository} from '../repositories';

export class FactureController {
  constructor(
    @repository(FactureRepository)
    public factureRepository : FactureRepository,
  ) {}

  @post('/factures', {
    responses: {
      '200': {
        description: 'Facture model instance',
        content: {'application/json': {schema: getModelSchemaRef(Facture)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Facture, {
            title: 'NewFacture',
            exclude: ['id'],
          }),
        },
      },
    })
    facture: Omit<Facture, 'id'>,
  ): Promise<Facture> {
    return this.factureRepository.create(facture);
  }

  @get('/factures/count', {
    responses: {
      '200': {
        description: 'Facture model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Facture)) where?: Where<Facture>,
  ): Promise<Count> {
    return this.factureRepository.count(where);
  }

  @get('/factures', {
    responses: {
      '200': {
        description: 'Array of Facture model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Facture, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Facture)) filter?: Filter<Facture>,
  ): Promise<Facture[]> {
    return this.factureRepository.find(filter);
  }

  @patch('/factures', {
    responses: {
      '200': {
        description: 'Facture PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Facture, {partial: true}),
        },
      },
    })
    facture: Facture,
    @param.query.object('where', getWhereSchemaFor(Facture)) where?: Where<Facture>,
  ): Promise<Count> {
    return this.factureRepository.updateAll(facture, where);
  }

  @get('/factures/{id}', {
    responses: {
      '200': {
        description: 'Facture model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Facture, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(Facture)) filter?: Filter<Facture>
  ): Promise<Facture> {
    return this.factureRepository.findById(id, filter);
  }

  @patch('/factures/{id}', {
    responses: {
      '204': {
        description: 'Facture PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Facture, {partial: true}),
        },
      },
    })
    facture: Facture,
  ): Promise<void> {
    await this.factureRepository.updateById(id, facture);
  }

  @put('/factures/{id}', {
    responses: {
      '204': {
        description: 'Facture PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() facture: Facture,
  ): Promise<void> {
    await this.factureRepository.replaceById(id, facture);
  }

  @del('/factures/{id}', {
    responses: {
      '204': {
        description: 'Facture DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.factureRepository.deleteById(id);
  }
}
