import {Entity, model, property} from '@loopback/repository';

@model()
export class Etudiant extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  cin?: number;

  @property({
    type: 'string',
  })
  nom?: string;

  @property({
    type: 'string',
  })
  promo?: string;


  constructor(data?: Partial<Etudiant>) {
    super(data);
  }
}

export interface EtudiantRelations {
  // describe navigational properties here
}

export type EtudiantWithRelations = Etudiant & EtudiantRelations;
