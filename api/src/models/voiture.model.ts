import {Entity, model, property} from '@loopback/repository';

@model()
export class Voiture extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  matricule?: number;

  @property({
    type: 'string',
  })
  marque?: string;


  constructor(data?: Partial<Voiture>) {
    super(data);
  }
}

export interface VoitureRelations {
  // describe navigational properties here
}

export type VoitureWithRelations = Voiture & VoitureRelations;
