import {Entity, model, property} from '@loopback/repository';

@model()
export class Facture extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  montant?: string;


  constructor(data?: Partial<Facture>) {
    super(data);
  }
}

export interface FactureRelations {
  // describe navigational properties here
}

export type FactureWithRelations = Facture & FactureRelations;
